vim.g.base46_cache = vim.fn.stdpath "data" .. "/base46/"
vim.g.mapleader = " "

-- bootstrap lazy and all plugins
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"

if not vim.uv.fs_stat(lazypath) then
  local repo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system { "git", "clone", "--filter=blob:none", repo, "--branch=stable", lazypath }
end

vim.opt.rtp:prepend(lazypath)

local lazy_config = require "configs.lazy"

-- load plugins
require("lazy").setup({
  {
    "NvChad/NvChad",
    lazy = false,
    branch = "v2.5",
    import = "nvchad.plugins",
  },
  {
    "hrsh7th/nvim-cmp",
    opts = {
      enabled = function()
        return false
      end,
    },
  },
  { import = "plugins" },
}, lazy_config)

-- load theme
dofile(vim.g.base46_cache .. "defaults")
dofile(vim.g.base46_cache .. "statusline")

require "options"
require "nvchad.autocmds"

vim.schedule(function()
  require "mappings"
end)

-- Here to the bottom converted to lua from vimrc --
vim.cmd("syntax on")
vim.opt.encoding = "utf-8"
vim.opt.expandtab = true
vim.opt.foldlevel = 99
vim.opt.foldmethod = "indent"
vim.opt.mouse = "v"
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabpagemax = 100
vim.opt.tabstop = 4

-- Compatibility mode for backspace key
vim.opt.backspace = "indent,eol,start"
-- Show the cursor position
vim.opt.ruler = true
-- Highlight search results
vim.opt.hlsearch = true
-- Enable filetype detection and indentation
vim.cmd("filetype indent on")
-- If terminal supports 256 colors, set terminal UI colors
if vim.fn.has("termguicolors") then
    vim.opt.termguicolors = true
end
