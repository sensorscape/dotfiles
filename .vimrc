" vimrc

set mouse=v
set encoding=utf-8
"let python_highlight_all=1
syntax on
"set t_Co=256
"set background=dark
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set backspace=2
set backspace=indent,eol,start
set ruler
set hlsearch
filetype indent on

nnoremap <SPACE> <Nop>
let mapleader=" "

" move split window
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>

" tabs
noremap <leader>c :tabnew<CR>
noremap <leader>n :tabn<CR>
noremap <leader>p :tabp<CR>
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>
noremap <Leader>w :w<CR>
noremap <Leader>e :Explore<CR>

if &term =~ '256color'
  set t_ut=
endif

" fix slow editing ruby files
set re=1

" allow to open many tabs at once
set tabpagemax=100

" Enable folding
set foldmethod=indent
set foldlevel=99

autocmd FileType yaml,yml autocmd BufWritePre <buffer> :%s/\($\n\s*\)\+\%$//e
