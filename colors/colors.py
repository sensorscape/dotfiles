#!/usr/bin/env python3

import os
from flask import Flask, request

app = Flask(__name__)
command = "timeout 10 /usr/local/bin/sudo /usr/local/bin/openrgb -d 0"

@app.route('/color', methods = ['POST'])
def color():
    print("REQ: {}".format(request.json))
    if request.method == 'POST':
        color = request.json.get('color')

        if color == 'black':
            os.system('{} --color 000000 -m static'.format(command))
        elif color == 'red':
            os.system('{} --color ff0000 -m static'.format(command))
        elif color == 'green':
            os.system('{} --color 00ff00 -m static'.format(command))
        elif color == 'yellow':
            os.system('{} --color ffff00 -m static'.format(command))
        elif color == 'blue':
            os.system('{} --color 0000ff -m static'.format(command))
        elif color == 'magenta':
            os.system('{} --color ff00ff -m static'.format(command))
        elif color == 'cyan':
            os.system('{} --color 00ffff -m static'.format(command))
        elif color == 'white':
            os.system('{} --color ffffff -m static'.format(command))

    return 'ok'

@app.route('/effect', methods = ['POST'])
def effect():
    if request.method == 'POST':
        effect = request.json.get('effect')
        color = request.json.get('color')
        if color == None:
            color = '00ffff'
        period = request.json.get('period')
        if period == None: # period format is xms or xs
            period = '1s'

        if effect == 'off':
            os.system('{} --mode Off --color {}'.format(command, color))
        elif effect == 'static':
            os.system('{} --mode Static --color {}'.format(command, color))
        elif effect == 'cycle':
            os.system('{} --mode Cycle --color {}'.format(command, color))
        elif effect == 'breathing':
            os.system('{} --mode Breathing --color {}'.format(command, color))

    return 'ok'
