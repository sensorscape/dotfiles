#!/bin/sh

if [ -z $1 ]; then
    echo "Must specify a host"
    exit
fi

if [ "$1" = "sftp" ]; then
    if [ -z $2 ]; then
        echo "host not specified"
        exit
    fi

    sftp -o port=1980 jd@$2
else
    ssh -o port=1980 jd@$1
fi
